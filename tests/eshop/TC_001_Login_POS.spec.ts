import { expect, test } from '@playwright/test';

test('TC_001_Login_POS', async ({ page }) => {
    const eshopUrl = 'https://automationexercise.com'
    await page.goto(eshopUrl)
    await expect(page).toHaveURL(eshopUrl)
    await expect(page).toHaveTitle('Automation Exercise')

    const signupLink = page.locator('a[href="/login"]')
    await signupLink.click()

    const loginLabel = page.getByText('Login to your account')
    await expect(loginLabel).toBeVisible()

    const emailInput = page.locator('input[data-qa="login-email"]')
    const passwordInput = page.locator('input[data-qa="login-password"]')
    const loginButton = page.locator('button[data-qa="login-button"]')

    await emailInput.fill('petrsobotka@email.cz')
    await passwordInput.fill('heslo1234')
    await loginButton.click()

    const userLabel = page.locator('//a[contains(text(),"Logged in as")]')
    await expect(userLabel).toContainText('Petr Sobotka')
})