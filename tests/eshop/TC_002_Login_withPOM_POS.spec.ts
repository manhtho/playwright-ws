import { expect } from "playwright/test"
import { test } from "../../fixture/testFixture"

test('TC_002_Login_withPOM_POS', async ({ navbar, homePage, loginPage, }) => {
    await test.step('1. Open eshop', async () => {
        await homePage.openEshopHomePage()
    })

    await test.step('2. Open login page', async () => {
        await navbar.openLoginPage()
        await expect(loginPage.loginLabel).toBeVisible()
    })

    await test.step('3. Login', async () => {
        await loginPage.login('petrsobotka@email.cz', 'heslo1234')
        await expect(navbar.userLabel).toContainText('Petr Sobotka')
    })
})