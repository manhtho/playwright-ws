import { expect, test } from '@playwright/test';

test('TC_002_User_API_POS', async ({ request }) => {
    const apiUrl = 'https://automationexercise.com'
    const response = await request.get(`${apiUrl}/api/getUserDetailByEmail?email=${'petrsobotka@email.cz'}`)
    const responseBody = JSON.parse(await response.text())

    expect(response.status()).toBe(200)
    console.log('response: %j', responseBody)
})