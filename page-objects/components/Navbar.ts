import { Locator, Page } from "@playwright/test";

export class Navbar {
    readonly signupLink: Locator
    readonly userLabel: Locator

    constructor(page: Page) {
        this.signupLink = page.locator('a[href="/login"]')
        this.userLabel = page.locator('//a[contains(text(),"Logged in as")]')
    }

    async openLoginPage() {
        await this.signupLink.click()
    }
}