import { Page, expect } from "@playwright/test";

export class HomePage {

    readonly page: Page

    constructor(page: Page) { this.page = page }

    async openEshopHomePage() {
        const eshopUrl = 'https://automationexercise.com'
        await this.page.goto(eshopUrl)
        await expect(this.page).toHaveURL(eshopUrl)
        await expect(this.page).toHaveTitle('Automation Exercise')
    }
}