import { Locator, Page } from "@playwright/test";

export class LoginPage {
    readonly loginLabel: Locator
    readonly emailInput: Locator
    readonly passwordInput: Locator
    readonly loginButton: Locator

    constructor(page: Page) {
        this.loginLabel = page.getByText('Login to your account')
        this.emailInput = page.locator('input[data-qa="login-email"]')
        this.passwordInput = page.locator('input[data-qa="login-password"]')
        this.loginButton = page.locator('button[data-qa="login-button"]')
    }

    async login(email: string, password: string) {
        await this.emailInput.fill('petrsobotka@email.cz')
        await this.passwordInput.fill('heslo1234')
        await this.loginButton.click()
    }
}