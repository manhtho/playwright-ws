import { test as testBase } from "@playwright/test"
import { Navbar } from "../page-objects/components/Navbar";
import { HomePage } from "../page-objects/HomePage";
import { LoginPage } from "../page-objects/LoginPage";

type pageFixture = {
    navbar: Navbar
    homePage: HomePage
    loginPage: LoginPage
}

export const test = testBase.extend<pageFixture>({
    navbar: async ({ page }, use) => {
        await use(new Navbar(page))
    },
    homePage: async ({ page }, use) => {
        await use(new HomePage(page))
    },
    loginPage: async ({ page }, use) => {
        await use(new LoginPage(page))
    },
})